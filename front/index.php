<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>SWR - BICU</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/custom.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/font-awesome.css">
</head>
<body>
<div class="container" class="row" id="main-container">
	<!-- Inicio navegador -->
	<?php
		include('../includes/nav.inc.php');
	?>
	<!-- Fin navegador -->
	<!--<div class="row">
		<div class="col-sm-12">
			
            <?php 
                session_start();
                if(isset($_SESSION['message'])){
                    ?>
                    <div class="alert alert-dismissible alert-success" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $_SESSION['message']; ?>
                    </div>
                    <?php

                    unset($_SESSION['message']);
                }
            ?>
		</div>
	</div>-->
	<div class="row row-offcanvas-right">
		<div class="horizontal-slider clearfix">
			<div class="primero">
				<div class="container text-center">
					<div class="dc_type_home">
						<div class="col-sm4 dc_type">
							<div class="numero_home_caja_">4</div>
							<div class="texto_home_caja"><a href="">Monografias</a></div>
						</div>
						<div class="col-sm4 dc_type">
							<div class="numero_home_caja_">4</div>
							<div class="texto_home_caja"><a href="">Proyectos</a></div>
						</div>
							<div class="numero_home_caja_">4</div>
							<div class="texto_home_caja"><a href="">Monografias Tecnicas</a></div>
						<div class="col-sm4 dc_type">
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>

<script src="bootstrap/js/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<script src="bootstrap/js/custom.js"></script>
</body>
</html>
