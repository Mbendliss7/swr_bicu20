<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>SWR - BICU</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/custom.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/font-awesome.css">
</head>
<body>
<div class="container">
	<!-- Inicio navegador -->
	<?php
		include('includes/nav.inc.php');
	?>
	<!-- Fin navegador -->
	<h1 class="page-header text-center">SWR - BICU</h1>
	<div class="row">
		<div class="col-sm-12">
			
            <?php 
                session_start();
                if(isset($_SESSION['message'])){
                    ?>
                    <div class="alert alert-dismissible alert-success" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $_SESSION['message']; ?>
                    </div>
                    <?php

                    unset($_SESSION['message']);
                }
            ?>
			<center>
				<h1>Bienbenidos a Sitio Web Referencial - BICU</h1>
				<img src="img/centro.png" alt="">
			</center>
		</div>
	</div>
</div>

<script src="bootstrap/js/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<script src="bootstrap/js/custom.js"></script>
</body>
</html>
