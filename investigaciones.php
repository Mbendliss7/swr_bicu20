<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>SWR - BICU</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/custom.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/font-awesome.css">
</head>
<body>
<div class="container">
	<!-- Inicio navegador -->
	<?php
		include('includes/nav.inc.php');
	?>
	<!-- Fin navegador -->
	<h1 class="page-header text-center">Lista de Investigaciones</h1>
	<div class="row">
		<div class="col-sm-12">
			<a href="#addnew" class="btn btn-primary" data-toggle="modal"><span class="fa fa-plus"></span> Nueva investigacion</a>
			<a href="#addnew_ac" class="btn btn-primary" data-toggle="modal"><span class="fa fa-plus"></span> Nuevo Acesor</a>
			<a href="#addnew_au" class="btn btn-primary" data-toggle="modal"><span class="fa fa-plus"></span> Nuevo Autor</a>
			<a href="#addnew_tu" class="btn btn-primary" data-toggle="modal"><span class="fa fa-plus"></span> Nuevo Tutor</a>
            <?php 
                session_start();
                if(isset($_SESSION['message'])){
                    ?>
                    <div class="alert alert-dismissible alert-success" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $_SESSION['message']; ?>
                    </div>
                    <?php

                    unset($_SESSION['message']);
                }
            ?>
			<table class="table table-bordered table-striped" style="margin-top:20px;">
				<thead>
					<th>ID</th>
					<th>Titulo</th>
					<th>Tutor</th>
					<th>Acesor</th>
					<th>Estado</th>
					<th>Acción</th>
				</thead>
				<tbody>
					<?php
						// incluye la conexión
						include_once('includes/connection.php');

						$database = new Connection();
    					$db_vinv = $database->open();
						try{	
						    $sql_vinv ='select investigaciones.id_invest as ID,investigaciones.titulo as Titulo, CONCAT(tutor.nombre,tutor.apellido)as Tutor,CONCAT(acesor.nombre,acesor.apellido) as Acesor,estado.tipo_esta as Estado 
							from investigaciones
							INNER JOIN tutor ON investigaciones.fk_tutor=tutor.id_tutor
							INNER JOIN estado ON investigaciones.fk_estado=estado.id_estado
							INNER JOIN acesor ON investigaciones.acesor_Id=acesor.id_acesor
							ORDER BY investigaciones.id_invest';
						    foreach ($db_vinv->query($sql_vinv) as $row_vinv) {
						    	?>
						    	<tr>
						    		<td><?php echo $row_vinv['ID']; ?></td>
						    		<td><?php echo $row_vinv['Titulo']; ?></td>
						    		<td><?php echo $row_vinv['Tutor']; ?></td>
									<td><?php echo $row_vinv['Acesor']; ?></td>
									<td><?php echo $row_vinv['Estado']; ?></td>
									<td>
						    			<a href="#edit_<?php echo $row_vinv['ID']; ?>" class="btn btn-success btn-sm" data-toggle="modal"><span class="fa fa-edit"></span> Editar</a>
						    			<a href="#delete_<?php echo $row_vinv['ID']; ?>" class="btn btn-danger btn-sm" data-toggle="modal"><span class="fa fa-trash"></span> Eliminar</a>
						    		</td>
						    		<?php include('includes/investigaciones/edit_delete_modal.php'); ?>
						    	</tr>
						    	<?php 
						    }
						}
						catch(PDOException $e){
							echo "There is some problem in connection: " . $e->getMessage();
						}

						$database->close();
						//cerrar conexión
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php include('includes/investigaciones/add_modal.php'); ?>
<script src="bootstrap/js/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<script src="bootstrap/js/custom.js"></script>
</body>
</html>
