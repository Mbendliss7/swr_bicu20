<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>SWR - BICU</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/custom.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/font-awesome.css">
</head>
<body>
<div class="container">
	<!-- Inicio navegador -->
	<?php
		include('includes/nav.inc.php');
	?>
	<!-- Fin navegador -->
	<h1 class="page-header text-center">Lista de Carreras</h1>
	<div class="row">
		<div class="col-sm-12">
			<a href="#addnew" class="btn btn-primary" data-toggle="modal"><span class="fa fa-plus"></span> Nuevo</a>
            <?php 
                session_start();
                if(isset($_SESSION['message'])){
                    ?>
                    <div class="alert alert-dismissible alert-success" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $_SESSION['message']; ?>
                    </div>
                    <?php

                    unset($_SESSION['message']);
                }
            ?>
			<table class="table table-bordered table-striped" style="margin-top:20px;">
				<thead>
                <th>ID</th>
				<th>Nombre</th>
				<th>Accion</th>
					
				</thead>
				<tbody>
					<?php
						// incluye la conexión
						include_once('includes/connection.php');

						$database = new Connection();
    					$db_car = $database->open();
						try{	
						    $sql_car = 'select 	idCarrera as ID, nombreCarrera as Nombre from carrera';
						    foreach ($db_car->query($sql_car) as $row_car) {
						    	?>
						    	<tr>
                                    <td><?php echo $row_car['ID']; ?></td>
                                    <td><?php echo $row_car['Nombre']; ?></td>
						    	 	<td>
						    		
						    			<a href="#edit_<?php echo $row_car['ID']; ?>" class="btn btn-success btn-sm" data-toggle="modal"><span class="fa fa-edit"></span> Editar</a>
						    			<a href="#delete_<?php echo $row_car['ID']; ?>" class="btn btn-danger btn-sm" data-toggle="modal"><span class="fa fa-trash"></span> Eliminar</a>
						    		</td>
						    		<?php include('includes/carrera/edit_delete_modal.php'); ?>
						    	</tr>
						    	<?php 
						    }
						}
						catch(PDOException $e){
							echo "There is some problem in connection: " . $e->getMessage();
						}

						//cerrar conexión
						$database->close();
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php include('includes/carrera/add_modal.php'); ?>
<script src="bootstrap/js/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<script src="bootstrap/js/custom.js"></script>
</body>
</html>
