<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>SWR - BICU</title>
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/custom.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/font-awesome.css">
</head>
<body>
<div class="container">
	<!-- Inicio navegador -->
	<?php
		include('includes/nav.inc.php');
	?>
	<!-- Fin navegador -->
	<h1 class="page-header text-center">Lista de Sugerencias</h1>
	<div class="row">
		<div class="col-sm-12">
			<a href="#addnew" class="btn btn-primary" data-toggle="modal"><span class="fa fa-plus"></span> Nuevo</a>
            <?php 
                session_start();
                if(isset($_SESSION['message'])){
                    ?>
                    <div class="alert alert-dismissible alert-success" style="margin-top:20px;">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                        <?php echo $_SESSION['message']; ?>
                    </div>
                    <?php

                    unset($_SESSION['message']);
                }
            ?>
			<table class="table table-bordered table-striped" style="margin-top:20px;">
				<thead>
                <th>ID</th>
				<th>Nombre</th>
				<th>Carrera</th>
				<th>Accion</th>
				
					
				</thead>
				<tbody>
					<?php
						// incluye la conexión
						include_once('includes/connection.php');

						$database = new Connection();
    					$db_sug = $database->open();
						try{	
						    $sql_sug = 'SELECT id_suge as ID, titulo as Titulo, carrera.nombreCarrera as Carrera FROM sugerencias INNER JOIN carrera ON sugerencias.fk_id_carrera=carrera.idCarrera';
						    foreach ($db_sug->query($sql_sug) as $row_sug) {
						    	?>
						    	<tr>
                                    <td><?php echo $row_sug['ID']; ?></td>
                                    <td><?php echo $row_sug['Titulo']; ?></td>
									<td><?php echo $row_sug['Carrera']; ?></td>
						    	 	<td>
						    		
						    			<a href="#edit_<?php echo $row_sug['ID']; ?>" class="btn btn-success btn-sm" data-toggle="modal"><span class="fa fa-edit"></span> Editar</a>
						    			<a href="#delete_<?php echo $row_sug['ID']; ?>" class="btn btn-danger btn-sm" data-toggle="modal"><span class="fa fa-trash"></span> Eliminar</a>
						    		</td>
						    		<?php include('includes/sugerencia/edit_delete_modal.php'); ?>
						    	</tr>
						    	<?php 
						    }
						}
						catch(PDOException $e){
							echo "There is some problem in connection: " . $e->getMessage();
						}

						//cerrar conexión
						$database->close();
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
<?php include('includes/sugerencia/add_modal.php'); ?>
<script src="bootstrap/js/jquery.min.js"></script>
<script src="bootstrap/js/bootstrap.js"></script>
<script src="bootstrap/js/custom.js"></script>
</body>
</html>
