<!-- Editar -->
<div class="modal fade" id="edit_<?php echo $row_car['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	 <center><h4 class="modal-title" id="myModalLabel">Editar Carrera</h4></center>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="includes/carrera/edit.php?id=<?php echo $row_car['ID']; ?>">
			<div class="row">
					<div class="col-sm-12"> <!-- Inicio de columna-->
						<div class="row form-group">
							<div class="col-sm-12">
								<label class="control-label" style="position:relative; top:7px;">Carrera:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="Nombre" value="<?php echo $row_car['Nombre']; ?>">
							</div>
						</div>
					</div> <!-- fin Columna -->
				</div>
            </div> 
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
                <button type="submit" name="edit" class="btn btn-success"><span class="fa fa-check"></span> Actualizar</a>
			</form>
            </div>

        </div>
    </div>
</div>

<!-- Eliminar -->
<div class="modal fade" id="delete_<?php echo $row_car['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	<center><h4 class="modal-title" id="myModalLabel">Borrar Carrera</h4></center>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">	
            	<p class="text-center">¿Estas seguro en borrar la Carrera?</p>
				<h2 class="text-center"><?php echo $row_car['Nombre']; ?></h2>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
                <a href="includes/carrera/delete.php?id=<?php echo $row_car['ID']; ?>" class="btn btn-danger"><span class="fa fa-trash"></span> Si</a>
            </div>

        </div>
    </div>
</div>
