<!-- Editar -->
<div class="modal fade bd-example-modal-lg" id="edit_<?php echo $row_vusr['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
            	 <center><h4 class="modal-title" id="myModalLabel">Editar usuario</h4></center>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="includes/usuarios/edit.php?id=<?php echo $row_vusr['ID']; ?>">
			<div class="row">
					<div class="col-sm-6"> <!-- Inicio de columna 1 -->
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Nombre:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="firstname" value="<?php echo $row_vusr['Nombre']; ?>">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Apellido:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="lastname" value="<?php echo $row_vusr['Apellido']; ?>">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Escuela:</label>
							</div>
							<div class="col-sm-10">
							<!-- 
								<input type="text" class="form-control" name="escuela">
								 -->
								<select class="form-control" id="selecttuser" name="escuela" value="<?php echo $row_vusr['escuela']; ?>">
									<option>Seleccione tipo de usuario</option>
									<?php
									$db_esc = $database->open();
									$sql_esc = "SELECT * FROM escuela";
									$result_esc = $db_esc->query($sql_esc);
									  while($fila_esc = $result_esc->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_esc['id_escuela']; ?>" > <?php echo $fila_esc['nombre']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
									
								</select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Facultad:</label>
							</div>
							<div class="col-sm-10">
								<!--  
								<input type="text" class="form-control" name="facultad">
								-->
								<select class="form-control" id="selectfacultad" name="facultad" value="<?php echo $row_vusr['fac']; ?>">
									<option>Seleccione la Facultad</option>
									<?php
									$db_Fac = $database->open();
									$sql_Fac = "SELECT * FROM facultad";
									$result_Fac = $db_Fac->query($sql_Fac);
									  while($fila_Fac = $result_Fac->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_Fac['id_facultad']; ?>"> <?php echo $fila_Fac['nombre']; ?> </option>
										<?php
									  }
									$database->close();
									?>
									
								</select>
							</div>
						</div>
					</div> <!-- fin Columna 1 -->
					<div class="col-sm-6"> <!-- inicio de columna 2 -->
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Tipo de Usuario:</label>
							</div>
							<div class="col-sm-10">
								<!-- 
								<input type="text" class="form-control" name="tuser">
								 -->
								<select class="form-control" id="selecttuser" name="tuser" value="<?php echo $row_vusr['tu']; ?>">
									<option>Seleccione tipo de usuario</option>
									<?php
									$db_usr = $database->open();
									$sql_usr = "SELECT * FROM tipo_usuario";
									$result_usr = $db_usr->query($sql_usr);
									  while($fila_usr = $result_usr->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_usr['tipo']; ?>" > <?php echo $fila_usr['nombre']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
									
								</select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Nombre de usuario:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="nickname" value="<?php echo $row_vusr['User']; ?>">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Contraseña:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="passd" value="<?php echo $row_vusr['pass']; ?>">
							</div>
						</div>
					</div> <!-- fin de columna 2 -->
				</div>
            </div> 
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
                <button type="submit" name="edit" class="btn btn-success"><span class="fa fa-check"></span> Actualizar</a>
			</form>
            </div>

        </div>
    </div>
</div>

<!-- Eliminar -->
<div class="modal fade" id="delete_<?php echo $row_vusr['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	<center><h4 class="modal-title" id="myModalLabel">Borrar miembro</h4></center>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">	
            	<p class="text-center">¿Estas seguro en borrar los datos de?</p>
				<h2 class="text-center"><?php echo $row_vusr['Nombre'].' '.$row_vusr['Apellido']; ?></h2>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
                <a href="includes/usuarios/delete.php?id=<?php echo $row_vusr['ID']; ?>" class="btn btn-danger"><span class="fa fa-trash"></span> Si</a>
            </div>

        </div>
    </div>
</div>
