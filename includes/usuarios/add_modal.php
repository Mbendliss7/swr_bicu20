<?php
	//include_once('connection.php');
	//$database = new Connection();
?>
<!-- Agregar Nuevo -->
<div class="modal fade bd-example-modal-lg" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            	<center><h4 class="modal-title" id="myModalLabel">Agregar Usuario</h4></center>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
                
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="includes/usuarios/add.php">
				<div class="row">
					<div class="col-sm-6"> <!-- Inicio de columna 1 -->
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Nombre:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="firstname">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Apellido:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="lastname">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Escuela:</label>
							</div>
							<div class="col-sm-10">
							<!-- 
								<input type="text" class="form-control" name="escuela">
								 -->
								<select class="form-control" id="selecttuser" name="escuela">
									<option>Seleccione la Escuela</option>
									<?php
									$db_esc = $database->open();
									$sql_esc = "SELECT * FROM escuela";
									$result_esc = $db_esc->query($sql_esc);
									  while($fila_esc = $result_esc->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_esc['id_escuela']; ?>" > <?php echo $fila_esc['nombre']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
									
								</select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Facultad:</label>
							</div>
							<div class="col-sm-10">
								<!--  
								<input type="text" class="form-control" name="facultad">
								-->
								<select class="form-control" id="selectfacultad" name="facultad">
									<option>Seleccione la Facultad</option>
									<?php
									$db_Fac = $database->open();
									$sql_Fac = "SELECT * FROM facultad";
									$result_Fac = $db_Fac->query($sql_Fac);
									  while($fila_Fac = $result_Fac->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_Fac['id_facultad']; ?>"> <?php echo $fila_Fac['nombre']; ?> </option>
										<?php
									  }
									$database->close();
									?>
									
								</select>
							</div>
						</div>
					</div> <!-- fin Columna 1 -->
					<div class="col-sm-6"> <!-- inicio de columna 2 -->
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Tipo de Usuario:</label>
							</div>
							<div class="col-sm-10">
								<!-- 
								<input type="text" class="form-control" name="tuser">
								 -->
								<select class="form-control" id="selecttuser" name="tuser">
									<option>Seleccione tipo de usuario</option>
									<?php
									$db_usr = $database->open();
									$sql_usr = "SELECT * FROM tipo_usuario";
									$result_usr = $db_usr->query($sql_usr);
									  while($fila_usr = $result_usr->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_usr['tipo']; ?>" > <?php echo $fila_usr['nombre']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
									
								</select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Nombre de usuario:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="nickname">
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Contraseña:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="passd">
							</div>
						</div>
					</div> <!-- fin de columna 2 -->
				</div>
            </div> 
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
                <button type="submit" name="add" class="btn btn-primary"><span class="fa fa-save"></span> Guardar</a>
			</form>
            </div>

        </div>
    </div>
</div>
