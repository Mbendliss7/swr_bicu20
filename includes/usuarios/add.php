<?php

	session_start();
	include_once('../connection.php');

	if(isset($_POST['add'])){
		$database = new Connection();
		$db = $database->open();
		try{
			// hacer uso de una declaración preparada para evitar la inyección de sql
			$stmt = $db->prepare("INSERT INTO usuario (nombre, apellido, escuela, facultad, tipo_usuario, usuario, contrasena) VALUES (:nombre, :apellido, :escuela, :facultad, :tipo_usuario, :usuario, :contrasena)");
			// declaración if-else en la ejecución de nuestra declaración preparada
			$_SESSION['message'] = ( $stmt->execute(array(':nombre' => $_POST['firstname'] , ':apellido' => $_POST['lastname'] , ':escuela' => $_POST['escuela'], ':facultad' => $_POST['facultad'], ':tipo_usuario' => $_POST['tuser'], ':usuario' => $_POST['nickname'], ':contrasena' => $_POST['passd'])) ) ? 'Miembro agregado correctamente' : 'Se dió un error';	
	    
		}
		catch(PDOException $e){
			$_SESSION['message'] = $e->getMessage();
		}

		//cerrar conexión
		$database->close();
	}

	else{
		$_SESSION['message'] = 'Fill up add form first';
	}

	header('location: ../../usuarios.php');
	
?>
