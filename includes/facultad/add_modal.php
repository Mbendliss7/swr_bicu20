<?php
	//include_once('connection.php');
	//$database = new Connection();
?>
<!-- Agregar Nuevo -->
<div class="modal fade bd-example" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
            	<center><h4 class="modal-title" id="myModalLabel">Agregar Nueva Facultad</h4></center>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
                
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="includes/facultad/add.php">
				<div class="row">
					<div class="col-sm-12"> <!--Inicio de columna-->
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Nombre de la Facultad:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="nombre">
							</div>
						</div>
					</div> <!--fin de Columna-->
				</div>
            </div> 
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
                <button type="submit" name="add" class="btn btn-primary"><span class="fa fa-save"></span> Guardar</a>
			</form>
            </div>

        </div>
    </div>
</div>
