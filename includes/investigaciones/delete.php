<?php

	session_start();
	include_once('../connection.php');

	if(isset($_GET['id'])){
		$database = new Connection();
		$db = $database->open();
		try{
			$sql = "DELETE FROM investigaciones WHERE id_invest = '".$_GET['id']."'";
			// declaración if-else en la ejecución de nuestra consulta
			$_SESSION['message'] = ( $db->exec($sql) ) ? 'Investigacion eliminada correctamente' : 'Ocurrió un error. No se pudo eliminar la investigacion';
		}
		catch(PDOException $e){
			$_SESSION['message'] = $e->getMessage();
		}

		//cerrar conexión
		$database->close();

	}
	else{
		$_SESSION['message'] = 'Seleccione Investigacion para eliminar';
	}

	header('location: ../../investigaciones.php');

?>