<div class="modal fade bd-example-modal-lg" id="addnew_ac" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
            	<center><h4 class="modal-title" id="myModalLabel">Agregar Acesor</h4></center>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		        <div class="modal-body">
		        	<div class="container-fluid">
		        		<form method="POST" action="includes/investigaciones/add_acesor.php">
		        			<div class="tab-content">
		        				<div class="row">
		        					<div class="col-sm-12">
		        						<div class="row form-group">
		        							<div class="col-sm-12">
		        								<label class="control-label" style="position:relative; top:7px;">Nombre:</label>
		        								<div class="col-sm-10">
													<input type="text" class="form-control" name="nombre">
												</div>
		        							</div>
		        						</div>
		        					</div>
		        				</div>
		        				<div class="col-sm-12">
		        						<div class="row form-group">
		        							<div class="col-sm-12">
		        								<label class="control-label" style="position:relative; top:7px;">Apellido:</label>
		        								<div class="col-sm-10">
													<input type="text" class="form-control" name="apellido">
												</div>
		        							</div>
		        						</div>
		        					</div>
		        					<div class="col-sm-12">
		        						<div class="row form-group">
		        							<div class="col-sm-12">
		        								<label class="control-label" style="position:relative; top:7px;">Sexo:</label>
		        								<div class="col-sm-4">
													<input type="text" class="form-control" name="sexo">
												</div>
		        							</div>
		        						</div>
		        					</div>
		        					<div class="modal-footer">
                						<button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
               							<button type="submit" name="add" class="btn btn-primary"><span class="fa fa-save"></span> Guardar</a></button>
           							</div>
		        				</div>
		        			</div>
		        		</div>
		        	</div>
            	</div>
            </div>
		</div>
	</div>
</div>
