<?php

	session_start();
	include_once('../connection.php');

	if(isset($_POST['add'])){
		$database = new Connection();
		$db = $database->open();
		try{

			//Autor
			// hacer uso de una declaración preparada para evitar la inyección de sql
			$stmt = $db->prepare("INSERT INTO autor (nombre_1, nombre_2, apellido_1, apellido_2,sexo, ano_egresado) 
			VALUES (:nombre_1, :nombre_2, :apellido_1, :apellido_2,:sexo, :ano_egresado)");
			// declaración if-else en la ejecución de nuestra declaración preparada
			$_SESSION['message'] = ( $stmt->execute(array(':nombre_1' => $_POST['pnombre'] , ':nombre_2' => $_POST['snombre'] , ':apellido_1' => $_POST['pape'], ':apellido_2' => $_POST['sape'],':sexo' => $_POST['sexo'], ':ano_egresado' => $_POST['egresado'])) ) ? 'autor agregado correctamente' : 'Se dió un error';	
			// Falta Sexo existe en la base de datos?
			//dentro dela tabla autor si agregalo en el sql de aquí Done!!genial
			//vamos a probarlo
			/*
			
			
			//carrera
			$stmt = $db->prepare("INSERT INTO carrera (nombreCarrera) VALUES (:nombreCarrera)");
			// declaración if-else en la ejecución de nuestra declaración preparada
			$_SESSION['message'] = ( $stmt->execute(array(':nombreCarrera' => $_POST['carrera'])) ) ? 'carrera agregada correctamente' : 'Se dió un error';	

			//escuela
			$stmt = $db->prepare("INSERT INTO escuela (nombre) VALUES (:nombre)");
			// declaración if-else en la ejecución de nuestra declaración preparada
			$_SESSION['message'] = ( $stmt->execute(array(':nombre' => $_POST['escuela'])) ) ? 'Escuela agregada correctamente' : 'Se dió un error';	
		*/
		}
		catch(PDOException $e){
			$_SESSION['message'] = $e->getMessage();
		}

		//cerrar conexión
		$database->close();
	}

	else{
		$_SESSION['message'] = 'Fill up add form first';
	}

	header('location: ../../investigaciones.php');
	
?>
