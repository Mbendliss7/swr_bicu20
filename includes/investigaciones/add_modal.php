<?php
	//include_once('connection.php');
	//$database = new Connection();
?>
<!-- Agregar Nuevo -->
<div class="modal fade bd-example-modal-lg" id="addnew" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
            	<center><h4 class="modal-title" id="myModalLabel">Agregar Investigacion</h4></center>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>  
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="includes/investigaciones/add.php">
			 <!--Navs Pills-->
				<div class="container">
				<ul class="nav nav-tabs" role="tablist">	
				<li class="nav-item">
				<a class="nav-link active" data-toggle="tab" href="#datos">Datos de la investigacion</a>
				</li>
				<li class="nav-item">
				<a class="nav-link " data-toggle="tab" href="#tutaces">Datos Tecnicos</a>
				</li>
				<li class="nav-item">
				<a class="nav-link " data-toggle="tab" href="#resobj">Resumen y Objetivos</a>
				</li>
				</ul>
					</div>
		<div class="tab-content">
			<div id="datos" class="container tab-pane active">
				<div class="row">
					<div class="col-sm-6"> <!-- Inicio de columna 1 -->
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Titulo:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="titulo">
							</div>
						</div>
						
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Inscripción:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="anoins">
							</div>
						</div>

						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Defensa:</label>
							</div>
							<div class="col-sm-10">
								<input type="text" class="form-control" name="anodef">
							</div>
						</div>

						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Estado:</label>
							</div>
							<div class="col-sm-10">
							<!-- 
								<input type="text" class="form-control" name="escuela">
								 -->
								<select class="form-control" id="selecttuser" name="estado">
									<option>Seleccione el Estado</option>
									<?php
									$db_vinv = $database->open();
									$sql_vinv = "SELECT * FROM estado";
									$result_vinv = $db_vinv->query($sql_vinv);
									  while($fila_vinv = $result_vinv->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_vinv['id_estado']; ?>"> <?php echo $fila_vinv['tipo_esta']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
								</select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Facultad:</label>
							</div>
							<div class="col-sm-10">
							<!-- 
								<input type="text" class="form-control" name="escuela">
								 -->
								<select class="form-control" id="selecttuser" name="fac">
									<option>Seleccione la Facultad</option>
									<?php
									$db_vinv = $database->open();
									$sql_vinv = "SELECT * FROM facultad";
									$result_vinv = $db_vinv->query($sql_vinv);
									  while($fila_vinv = $result_vinv->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_vinv['id_facultad']; ?>" > <?php echo $fila_vinv['nombre']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
								</select>
							</div>
						</div>

						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Escuela:</label>
							</div>
							<div class="col-sm-10">
							<!-- 
								<input type="text" class="form-control" name="escuela">
								 -->
								<select class="form-control" id="selecttuser" name="escuela">
									<option>Seleccione la Escuela</option>
									<?php
									$db_vinv = $database->open();
									$sql_vinv = "SELECT * FROM escuela";
									$result_vinv = $db_vinv->query($sql_vinv);
									  while($fila_vinv = $result_vinv->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_vinv['id_escuela']; ?>" > <?php echo $fila_vinv['nombre']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
								</select>
							</div>
						</div>
					</div> <!-- fin Columna 1 -->
					<div class="col-sm-6"> <!-- inicio de columna 2 -->
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Modalidad:</label>
							</div>
							<div class="col-sm-10">
								<!-- 
								<input type="text" class="form-control" name="tuser">
								 -->
								<select class="form-control" id="selecttuser" name="modal">
									<option>Seleccione la Modalidad</option>
									<?php
									$db_vinv = $database->open();
									$sql_vinv = "SELECT * FROM modalidad";
									$result_vinv = $db_vinv->query($sql_vinv);
									  while($fila_vinv = $result_vinv->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_vinv['id_modal']; ?>" > <?php echo $fila_vinv['tipo_modal']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
									
								</select>
							</div>
						</div>
						
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Fuente de financiamiento:</label>
							</div>
							<div class="col-sm-10">
								<!-- 
								<input type="text" class="form-control" name="tuser">
								 -->
								<select class="form-control" id="selecttuser" name="fufinan">
									<option>Seleccione Fuente de financiamiento</option>
									<?php
									$db_aut = $database->open();
									$sql_aut = "SELECT * FROM fuentefinan";
									$result_aut = $db_aut->query($sql_aut);
									  while($fila_aut = $result_aut->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_aut['id_finan']; ?>" > <?php echo $fila_aut['tipo_finan']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
									
								</select>
						</div>
					</div>
					<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Carrera:</label>
							</div>
							<div class="col-sm-10">
								<!-- 
								<input type="text" class="form-control" name="tuser">
								 -->
								<select class="form-control" id="selecttuser" name="carrera">
									<option>Seleccione la Carrera</option>
									<?php
									$db_aut = $database->open();
									$sql_aut = "SELECT * FROM carrera";
									$result_aut = $db_aut->query($sql_aut);
									  while($fila_aut = $result_aut->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_aut['idCarrera']; ?>" > <?php echo $fila_aut['nombreCarrera']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
									
								</select>
							</div>
						</div>
						<div ssclass="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Recinto:</label>
							</div>
							<div class="col-sm-10">
							<!-- 
								<input type="text" class="form-control" name="escuela">
								 -->
								<select class="form-control" id="selecttuser" name="recinto">
									<option>Seleccione el Recinto</option>
									<?php
									$db_vinv = $database->open();
									$sql_vinv = "SELECT * FROM recinto";
									$result_vinv = $db_vinv->query($sql_vinv);
									  while($fila_vinv = $result_vinv->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_vinv['id_recinto']; ?>" > <?php echo $fila_vinv['n_recinto']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
								</select>
							</div>
						</div>
					</div> <!-- fin de columna 2 -->
				</div>
				</div>
			
			<!--Tutores y autores-->
				<div id="tutaces" class="container tab-pane fade">
			<div class="row">
					<div class="col-sm-6"> <!-- Inicio de columna 1 -->
					<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Tutor:</label>
							</div>
							<div class="col-sm-10">
							
								<!--<input type="text" id="tutor" autocomplete="off" placeholder="Escibe nombre del Tutor" class="form-control" name="tutor" >-->
							

								<select class="form-control" id="selecttuser" name="tutor">
									<option>Seleccione el Tutor</option>
									<?php
									$db_vinv = $database->open();
									$sql_vinv = "SELECT * FROM tutor";
									$result_vinv = $db_vinv->query($sql_vinv);
									  while($fila_vinv = $result_vinv->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_vinv['id_tutor']; ?>" > <?php echo $fila_vinv['nombre']; ?>
											<?php echo $fila_vinv['apellido']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
								</select>
								<!--
								<a href="#addnew_tu" class="btn btn-primary" data-toggle="modal"><span class="fa fa-plus"></span> Nuevo</a>
								-->
							</div>
						</div>
						
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Autor:</label>
							</div>
							<div class="col-sm-10">
							<!-- 
								<input type="text" class="form-control" name="escuela">
								 -->
								<select class="form-control" id="selecttuser" name="pnombre">
									<option>Seleccione el Autor</option>
									<?php
									$db_aut = $database->open();
									$sql_aut = "SELECT * FROM autor";
									$result_aut = $db_aut->query($sql_aut);
									  while($fila_aut = $result_aut->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_aut['idAutor']; ?>" > <?php echo $fila_aut['nombre_1']; ?> <?php echo $fila_aut['nombre_2']; ?>
											 <?php echo $fila_aut['apellido_1']; ?> <?php echo $fila_aut['apellido_2']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
								</select>
								<!--
								<a href="#addnew_au" class="btn btn-primary" data-toggle="modal"><span class="fa fa-plus"></span> Nuevo</a>
							-->
							</div>
						</div>
					</div> <!-- fin Columna 1 -->
					<div class="col-sm-6"> <!-- inicio de columna 2 -->
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Acesor:</label>
							</div>
							<div class="col-sm-10">
								<!-- 
								<input type="text" class="form-control" name="tuser">
								 -->
								<select class="form-control" id="selecttuser" name="acesor">
									<option>Seleccione el Acesor</option>
									<?php
									$db_vinv = $database->open();
									$sql_vinv = "SELECT * FROM acesor";
									$result_vinv = $db_vinv->query($sql_vinv);
									  while($fila_vinv = $result_vinv->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_vinv['id_acesor']; ?>" > <?php echo $fila_vinv['nombre']; ?> <?php echo $fila_vinv['apellido']; ?></option>
										<?php
									  }
									  $database->close();
									?>
								</select>
								<!--
								<a href="#addnew_ac" class="btn btn-primary" data-toggle="modal"><span class="fa fa-plus"></span> Nuevo</a>
							-->
							</div>
						</div>
					</div> <!-- fin de columna 2 -->
			</div>
			</div>

			<!-- Resumen y Objetivo-->
			    <div id="resobj" class="container tab-pane fade">
			<div class="row">
					<div class="col-sm-12"> <!-- Inicio de columna 1 -->
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Resumen:</label>
							</div>
							<div class="col-sm-10">
								<textarea type="text" class="form-control" name="resumen"></textarea>
							</div>
						</div>
						
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label" style="position:relative; top:7px;">Objetivos:</label>
							</div>
							<div class="col-sm-10">
							
								<textarea type="text" class="form-control" name="objetivos"></textarea>	
							</div>
						</div>		
					</div> <!-- fin formualaio-->
			</div>
			</div>
			</div>

        </div> 
	</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
                <button type="submit" name="add" class="btn btn-primary"><span class="fa fa-save"></span> Guardar</a>
			</form>
            </div>

        </div>
    </div>
</div>

<?php include('includes/investigaciones/new_modal.php'); ?>
<?php include('includes/investigaciones/add_autor.php'); ?>
<?php include('includes/investigaciones/add_acesor.php'); ?>

