<?php

	session_start();
	include_once('../connection.php');

	if(isset($_POST['add'])){
		$database = new Connection();
		$db = $database->open();
		try{

			// hacer uso de una declaración preparada para evitar la inyección de sql
			
			$stmt = $db->prepare("INSERT INTO investigaciones (titulo, ano_de_inscripcion, ano_de_defensa, resumen, objetivos, modal_fk, fk_estado, acesor_Id, fk_finanFinan, fk_tutor, fk_facultad, fk_recinto, fk_escuela) 
			VALUES (:titulo, :ano_de_inscripcion, :ano_de_defensa, :resumen, :objetivos, :modal_fk, :fk_estado, :acesor_Id, :fk_finanFinan, :fk_tutor, :fk_facultad, :fk_recinto, :fk_escuela)");
			// declaración if-else en la ejecución de nuestra declaración preparada
			$_SESSION['message'] = ( $stmt->execute(array(':titulo' => $_POST['titulo'] , ':ano_de_inscripcion' => $_POST['anoins'] , ':ano_de_defensa' => $_POST['anodef'], ':resumen' => $_POST['resumen'], ':objetivos' => $_POST['objetivos'], ':modal_fk' => $_POST['modal'], ':fk_estado' => $_POST['estado'], ':acesor_Id' => $_POST['acesor'], ':fk_finanFinan' => $_POST['fufinan'], ':fk_tutor' => $_POST['tutor'], ':fk_facultad' => $_POST['fac'],  ':fk_escuela' => $_POST['escuela'], ':fk_recinto' => $_POST['recinto'])) ) ? 'investigacion agregada correctamente' : 'Se dió un error';	
			
			/*
			//Autor
			// hacer uso de una declaración preparada para evitar la inyección de sql
			$stmt = $db->prepare("INSERT INTO autor (nombre_1, nombre_2, apellido_1, apellido_2, ano_egresado) 
			VALUES (:nombre_1, :nombre_2, :apellido_1, :apellido_2, :ano_egresado)");
			// declaración if-else en la ejecución de nuestra declaración preparada
			$_SESSION['message'] = ( $stmt->execute(array(':nombre_1' => $_POST['pnombre'] , ':nombre_2' => $_POST['snombre'] , ':apellido_1' => $_POST['pape'], ':apellido_2' => $_POST['sape'], ':ano_egresado' => $_POST['egresado'])) ) ? 'autor agregado correctamente' : 'Se dió un error';	
			
			//carrera
			$stmt = $db->prepare("INSERT INTO carrera (nombreCarrera) VALUES (:nombreCarrera)");
			// declaración if-else en la ejecución de nuestra declaración preparada
			$_SESSION['message'] = ( $stmt->execute(array(':nombreCarrera' => $_POST['carrera'])) ) ? 'carrera agregada correctamente' : 'Se dió un error';	

			//escuela
			$stmt = $db->prepare("INSERT INTO escuela (nombre) VALUES (:nombre)");
			// declaración if-else en la ejecución de nuestra declaración preparada
			$_SESSION['message'] = ( $stmt->execute(array(':nombre' => $_POST['escuela'])) ) ? 'Escuela agregada correctamente' : 'Se dió un error';	
		*/
		}
		catch(PDOException $e){
			$_SESSION['message'] = $e->getMessage();
		}

		//cerrar conexión
		$database->close();
	}

	else{
		$_SESSION['message'] = 'Fill up add form first';
	}

	header('location: ../../investigaciones.php');
	
?>
