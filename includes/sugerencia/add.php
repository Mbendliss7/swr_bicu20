<?php

	session_start();
	include_once('../connection.php');

	if(isset($_POST['add'])){
		$database = new Connection();
			$db = $database->open();
			try{
			// hacer uso de una declaración preparada para evitar la inyección de sql
			$stmt = $db->prepare("INSERT INTO sugerencias (titulo, fk_id_carrera, fecha) VALUES (:titulo, :fk_id_carrera, :fecha)");
			// declaración if-else en la ejecución de nuestra declaración preparada
			$_SESSION['message'] = ( $stmt->execute(array(':titulo' => $_POST['Titulo'],':fk_id_carrera' => $_POST['carrera'], ':fecha' => $_POST['fecha'])) ) ? 'Sugerencia agregada correctamente' : 'Se dió un error';	
	    
		}
		catch(PDOException $e){
			$_SESSION['message'] = $e->getMessage();
		}

		//cerrar conexión
		$database->close();
	}

	else{
		$_SESSION['message'] = 'Fill up add form first';
	}

	header('location: ../../sugerencia.php');
	
?>
