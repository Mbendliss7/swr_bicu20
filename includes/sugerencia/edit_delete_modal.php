<!-- Editar -->
<div class="modal fade" id="edit_<?php echo $row_sug['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	 <center><h4 class="modal-title" id="myModalLabel">Editar Sugerencia</h4></center>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">
			<div class="container-fluid">
			<form method="POST" action="includes/sugerencia/edit.php?id=<?php echo $row_sug['ID']; ?>">
			<div class="row">
					<div class="col-sm-12"> <!-- Inicio de columna-->
						<div class="row form-group">
							<div class="col-sm-12">
								<label class="control-label" style="position:relative; top:7px;">Sugerencia:</label>
							</div><!-- Label -->
							<div class="col-sm-12">
								<input type="text" class="form-control" name="Titulo" value="<?php echo $row_sug['Titulo']; ?>">
							</div><!-- input-->
						</div><!-- Fin del Form Sugerencia-->

						<div class="row form-group">
							<div class="col-sm-12">
								<label class="control-label" style="position:relative; top:7px;">Carrera:</label>
							</div>
							<div class="col-sm-12">
							<!-- 
								<input type="text" class="form-control" name="escuela">
								 -->
								<select class="form-control" id="seleccarrera" name="Carrera">
									<option>Seleccione la Carrera</option>
									<?php
									$db_sug = $database->open();
									$sql_sug = "SELECT * FROM carrera";
									$result_sug = $db_sug->query($sql_sug);
									  while($fila_sug = $result_sug->fetch()) 
									  {
										?>
											<option value="<?php echo $fila_sug['idCarrera']; ?>" > <?php echo $fila_sug['nombreCarrera']; ?> </option>
										<?php
									  }
									  $database->close();
									?>
								</select>
							</div>
						</div>
					</div> <!-- fin Columna -->
				</div>
            </div> 
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
                <button type="submit" name="edit" class="btn btn-success"><span class="fa fa-check"></span> Actualizar</a>
			</form>
            </div>

        </div>
    </div>
</div>

<!-- Eliminar -->
<div class="modal fade" id="delete_<?php echo $row_sug['ID']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
            	<center><h4 class="modal-title" id="myModalLabel">Borrar Sugerencia</h4></center>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            </div>
            <div class="modal-body">	
            	<p class="text-center">¿Estas seguro en borrar la Sugerencia?</p>
				<h2 class="text-center"><?php echo $row_sug['Titulo']; ?></h2>
			</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="fa fa-close"></span> Cancelar</button>
                <a href="includes/sugerencia/delete.php?id=<?php echo $row_sug['ID']; ?>" class="btn btn-danger"><span class="fa fa-trash"></span> Si</a>
            </div>

        </div>
    </div>
</div>
